﻿//
// MIT License:
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED ""AS IS"", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Linguistics;
using Microsoft.ProjectOxford.Linguistics.Contract;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Echovoice.JSON;
using System.Json;



namespace Microsoft.ProjectOxford.Linguistics.Sample
{
    class Program
    {
        /// <summary>
        /// Initializes a new instance of <see cref="LinguisticsClient"/> class.
        /// </summary>
        private static readonly LinguisticsClient Client = new LinguisticsClient("a04619ebdb8e4fd4b97b504e0a5b9b72");

        /// <summary>
        /// Default jsonserializer settings
        /// </summary>
        private static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings()
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        static void Main(string[] args)
        {
            // List analyzers
            Analyzer[] supportedAnalyzers = null;
            try
            {
                supportedAnalyzers = ListAnalyzers();
                var analyzersAsJson = JsonConvert.SerializeObject(supportedAnalyzers, Formatting.Indented, jsonSerializerSettings);
               // Console.WriteLine("Supported analyzers: " + analyzersAsJson);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Failed to list supported analyzers: " + e.ToString());
                Environment.Exit(1);
            }

            // Analyze text with all available analyzers
            var analyzeTextRequest = new AnalyzeTextRequest()
            {
                Language = "en",
                AnalyzerIds = supportedAnalyzers.Select(analyzer => analyzer.Id).ToArray(),
                Text = "I'm not gonna lie you anymore"
            };

            try
            {
                var analyzeTextResults = AnalyzeText(analyzeTextRequest);                
                var resultsAsJson2 = JsonConvert.SerializeObject(analyzeTextResults[2].Result, Formatting.Indented, jsonSerializerSettings);
                var resultsAsJson = JsonConvert.SerializeObject(analyzeTextResults[0].Result, Formatting.None, jsonSerializerSettings);
                #region GiainenVBP
                string[] result = JSONDecoders.DecodeJsStringArray(resultsAsJson);
                result[0]= result[0].TrimStart('[', '"').TrimEnd('"');                
                int i=1;
                for(;;i++)
                {
                    if (result[i].IndexOf(@"""") != -1)
                    {
                        result[i] = result[i].TrimEnd('"');
                        break;
                    }
                }                
                for (int k=0;k<=i;k++)
                {
                    Console.WriteLine(result[k]);
                }
                #endregion
                Receiver[] tmp = JsonConvert.DeserializeObject<Receiver[]>(resultsAsJson2);
                
                Console.WriteLine(resultsAsJson2);    
                                       
                Console.WriteLine("Done.");

            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Failed to list supported analyzers: " + e.ToString());
                Environment.Exit(1);
            }

            Console.ReadKey();
        }

        /// <summary>
        /// List analyzers synchronously.
        /// </summary>
        /// <returns>An array of supported analyzers.</returns>
        private static Analyzer[] ListAnalyzers()
        {
            try
            {
                return Client.ListAnalyzersAsync().Result;
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to gather list of analyzers", exception as ClientException);
            }
        }

        /// <summary>
        /// Analyze text synchronously.
        /// </summary>
        /// <param name="request">Analyze text request.</param>
        /// <returns>An array of analyze text result.</returns>
        private static AnalyzeTextResult[] AnalyzeText(AnalyzeTextRequest request)
        {
            try
            {
                return Client.AnalyzeTextAsync(request).Result;
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to analyze text", exception as ClientException);
            }
        }
    }
    public class Receiver
    {
        public int Len { get; set; }
        public int Offset { get; set; }
        public Tokens[] Tokens { get; set; }
        
    }
    public class Tokens
    {
        public int Len { get; set; }
        public string NormalizedToken { get; set; }
        public int Offset { get; set; }
        public string RawToken { get; set; }
    }
}
