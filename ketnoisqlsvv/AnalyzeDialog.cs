﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;


namespace ketnoisqlsvv
{
    public class AnalyzeDialog
    {               
        private string sql;
        private SqlCommand cmd;
        float temp=0;
        private SqlDataReader DR;
        private float weightSum = 0;
        private int markPara = 0;
        private SqlConnection connect = new SqlConnection(@ConfigurationManager.AppSettings["DataStringConnection"]);
        public AnalyzeDialog(string para)
        {
            WeightSen = new float[50, 50];
            WeightSumSen = new float[50];
            MarkSenPercentage = new int[50];
            MarkSen = new float[50];
            for (int i = 0;i<50;i++)
            {
                WeightSumSen[i] = 0;
            }
            Sentences = para.Split('\n');
            this.LineCount = para.Split('\n').Length;
            AnalyzedPara = new AnalyzeResult[LineCount];
            for (int k=0;k<LineCount;k++)
            {                
                AnalyzedPara[k] = new AnalyzeResult(filterString(Sentences[k]));
                
                for (int j=0;j<AnalyzedPara[k].WordCount;j++)
                {
                    connect.Open();
                    sql = "SELECT TOP 1 Weight FROM WeightTag WHERE Tag='" + AnalyzedPara[k].type[j] + "' ORDER BY NEWID()";
                    cmd = new SqlCommand(sql, connect);
                    DR = cmd.ExecuteReader();
                    while (DR.Read())
                    {
                        weightSum += Convert.ToSingle(DR[0]);    
                        WeightSumSen[k]+= Convert.ToSingle(DR[0]);
                        WeightSen[k,j] = Convert.ToSingle(DR[0]);             
                    }
                    connect.Close();
                }
            }
            WeightSum = weightSum;
            Sentence = para;
        }
        String filterString(String s)
        {
            s = Regex.Replace(s, "\\,", "");
            s = Regex.Replace(s, "\\.", "");
            s = Regex.Replace(s, "\\?", "");
            s = Regex.Replace(s, "\\:", "");
            s = Regex.Replace(s, "\"", "");
            return s;
        }

        public string[] Sentences { get; set; }
        public string Sentence { get; set; }
        public int LineCount { get; set; }
        public float WeightSum { get; set; }
        public float[] WeightSumSen { get; set; }
        public AnalyzeResult[] AnalyzedPara { get; set; }
        public float[,] WeightSen { get; set; }
        public int MarkPara
        {
            get
            {
                return markPara;
            }
            set
            {                
                for(int i=0;i<LineCount;i++)
                {
                    temp += this.MarkSen[i];                                        
                }
                markPara = (int) (temp*100/WeightSum);
                temp = 0;
            }
        }
        public float[] MarkSen { get; set; }
        public int[] MarkSenPercentage { get; set; }

        public int markSentence(int line,string voice, float conf)
        {
            AnalyzeResult voice_res;
            int i = 0, j = 0;
            float result = 0;            
            int i_prev = -1, j_prev = -1;                    
            voice_res = new AnalyzeResult(filterString(voice));            
            int comp = AnalyzedPara[line].WordCount - voice_res.WordCount;
            i = 0;
            while (i < AnalyzedPara[line].WordCount && j < voice_res.WordCount)
            {
                if (String.Equals(AnalyzedPara[line].Tokens[i], voice_res.Tokens[j], StringComparison.OrdinalIgnoreCase))
                {
                    result += WeightSen[line,i];
                    i++;
                    j++;
                }
                else if (i_prev != i)
                {
                    result -= WeightSen[line, i];
                    if (i < AnalyzedPara[line].WordCount-1)
                    {
                        if (String.Equals(AnalyzedPara[line].Tokens[i + 1], voice_res.Tokens[j], StringComparison.OrdinalIgnoreCase))
                        {
                            i++;
                        }
                        else
                        {
                            i_prev = i;
                            j_prev = ++j;
                        }
                    }
                    else
                    {
                        i_prev = i;
                        j_prev = ++j;
                    }
                }
                else
                {
                    j++;
                }
                if (j == voice_res.WordCount)
                {
                    i++;
                    j = j_prev;
                }
            }
            //}            
            result = result < 0 ? 0 : result; //âm điểm thì cho 0
            result *= conf;            
            MarkSenPercentage[line] = (int) (result/WeightSumSen[line]*100);
            MarkSen[line] = result;
            return MarkSenPercentage[line];
        }

        public string SentenceInDialog()
        {
            string result="";
            for (int i =0;i<LineCount;i++)
            {
                if (i%2!=0)
                {
                    result += "Peter: ";
                    result += Sentences[i];
                    result += "\n";
                }
                else
                {
                    result += "David: ";
                    result += Sentences[i];
                    result += "\n";
                }                              
            }            
            return result;
        }
    }
}
