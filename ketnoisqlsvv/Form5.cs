﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace ketnoisqlsvv
{
    public partial class Form5 : Form
    {
        SqlConnection connect;
        public Form5()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Local Database Files"));
            connect = new SqlConnection(@ConfigurationManager.AppSettings["DataStringConnection"]);
            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;           
            this.comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            string sql = "select distinct Module from QuestionByWord order by Module asc";
            connect.Open();
            SqlCommand cmd = new SqlCommand(sql, connect);
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox2.Items.Add(DR[0]);

            }
            browseFile.Visible = false;
            browseT.Visible = false;
            questionBox.Multiline = false;
            connect.Close();
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addQuestion_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null || comboBox2.SelectedItem==null || questionBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Yêu cầu nhập đầy đủ thông tin");
                return;
            }
            if (comboBox1.SelectedIndex == 0)
            {
                if (openFileDialog1.CheckFileExists)
                {
                    connect.Open();
                    string sql = "insert into QuestionByWord(Tu, Module, URL) values ('" + questionBox.Text + "', '" + comboBox2.Text + "', '..\\..\\..\\DataWavFiles\\" + questionBox.Text + ".wav')";
                    SqlCommand cmd = new SqlCommand(sql, connect);
                    cmd.ExecuteScalar();
                    MessageBox.Show("Thêm câu hỏi thành công !");
                    connect.Close();
                    System.IO.File.Copy(openFileDialog1.FileName, "..\\..\\..\\DataWavFiles\\" + questionBox.Text+".wav");
                }      
                else
                {
                    MessageBox.Show("Yêu cầu nhập đầy đủ thông tin");
                }                       
            }
            if (comboBox1.SelectedIndex == 1)
            {
                connect.Open();
                string s;
                s = questionBox.Text.Replace("'", @"''");
                string sql = "insert into QuestionBySentence(Tu, Module) values ('" + s + "', '" + comboBox2.Text + "')";
                SqlCommand cmd = new SqlCommand(sql, connect);
                cmd.ExecuteScalar();
                MessageBox.Show("Thêm câu hỏi thành công !");
                connect.Close();
            }
            if (comboBox1.SelectedIndex == 2)
            {
                connect.Open();
                string s;
                s = questionBox.Text.Replace("'", @"''");
                s = s.Replace("\\", "'\\");
                string sql = "insert into QuestionByDialog(Tu, Module) values ('" + s + "', '" + comboBox2.Text + "')";
                SqlCommand cmd = new SqlCommand(sql, connect);
                cmd.ExecuteScalar();
                MessageBox.Show("Thêm câu hỏi thành công !");
                connect.Close();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void browseFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.StreamReader sr = new
                   System.IO.StreamReader(openFileDialog1.FileName);
                MessageBox.Show(openFileDialog1.FileName);
                sr.Close();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex==0)
            {
                browseFile.Visible = true;
                browseT.Visible = true;
                questionBox.Multiline =false;
            }
            if (comboBox1.SelectedIndex == 1)
            {
                browseFile.Visible = false;
                browseT.Visible = false;
                questionBox.Multiline = false;
            }
            if (comboBox1.SelectedIndex == 2)
            {
                browseFile.Visible = false;
                browseT.Visible = false;
                questionBox.Multiline = true;
            }            
        }
    }
}
