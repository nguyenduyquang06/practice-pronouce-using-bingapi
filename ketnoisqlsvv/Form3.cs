﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ketnoisqlsvv
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        public Form3(string username)
        {
            InitializeComponent();
            welcomeLabel.Text = username;
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void wordButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm = new Form4(0);
            frm.Show();
        }

        private void addQuestion_Click(object sender, EventArgs e)
        {
            
            Form5 frm = new Form5();
            frm.Show();
        }

        private void sentenceButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm = new Form4(1);
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm = new Form4(2);
            frm.Show();
        }
    }
}
