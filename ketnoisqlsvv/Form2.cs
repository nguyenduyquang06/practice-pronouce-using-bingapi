﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace ketnoisqlsvv
{
    public partial class Form2 : Form
    {
        SqlConnection connect = new SqlConnection(@ConfigurationManager.AppSettings["DataStringConnection"]);
        public Form2()
        {
            InitializeComponent();
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            string username = loginTex.Text;
            string password = passTex.Text;
            string levelRight = levelBox.Text;
            int rightLevel = 1;
            switch (levelRight)
            {
                case "THƯỜNG":
                    rightLevel = 0;
                    break;
                case "VIP1":
                    rightLevel = 1;
                    break;
                case "VIP2":
                    rightLevel = 2;
                    break;
                case "VIP3":
                    rightLevel = 3;
                    break;
                case "ADMIN":
                    rightLevel = 4;
                    break;
            }
            connect.Open();
            string sql = "select TenDangNhap from TaiKhoan where TenDangNhap='" + username + "'";
            SqlCommand cmd = new SqlCommand(sql, connect);            
            Object result = cmd.ExecuteScalar();
            if (result == null)
            {
                // Handle the empty result set case    
                sql = "insert into TaiKhoan(TenDangNhap, MatKhau, LoaiTaiKhoan) values ('"+username+"', '"+password+"', '"+rightLevel+"')";
                cmd = new SqlCommand(sql, connect);
                cmd.ExecuteScalar();
                MessageBox.Show("Đăng ký thành công tài khoản " + username);
            }
            else
            {                           
                MessageBox.Show("Tên đăng nhập đã có người sử dụng, vui lòng chọn tên khác");
            }
            connect.Close();
        }
    }
}
