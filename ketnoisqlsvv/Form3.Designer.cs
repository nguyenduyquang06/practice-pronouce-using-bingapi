﻿namespace ketnoisqlsvv
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.welcomeLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.wordButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.quitButton = new System.Windows.Forms.Button();
            this.sentenceButton = new System.Windows.Forms.Button();
            this.addQuestion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // welcomeLabel
            // 
            this.welcomeLabel.AutoSize = true;
            this.welcomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.welcomeLabel.Location = new System.Drawing.Point(-4, 26);
            this.welcomeLabel.Name = "welcomeLabel";
            this.welcomeLabel.Size = new System.Drawing.Size(60, 25);
            this.welcomeLabel.TabIndex = 0;
            this.welcomeLabel.Text = "Chào";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label2.Location = new System.Drawing.Point(-4, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(293, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Chọn module bài học ở bên dưới";
            // 
            // wordButton
            // 
            this.wordButton.Location = new System.Drawing.Point(106, 147);
            this.wordButton.Name = "wordButton";
            this.wordButton.Size = new System.Drawing.Size(75, 43);
            this.wordButton.TabIndex = 2;
            this.wordButton.Text = "Luyện nói theo từ";
            this.wordButton.UseVisualStyleBackColor = true;
            this.wordButton.Click += new System.EventHandler(this.wordButton_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(197, 91);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 47);
            this.button2.TabIndex = 3;
            this.button2.Text = "Luyện theo đoạn hội thoại";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(197, 226);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(75, 23);
            this.quitButton.TabIndex = 5;
            this.quitButton.Text = "Thoát";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // sentenceButton
            // 
            this.sentenceButton.Location = new System.Drawing.Point(12, 93);
            this.sentenceButton.Name = "sentenceButton";
            this.sentenceButton.Size = new System.Drawing.Size(75, 43);
            this.sentenceButton.TabIndex = 6;
            this.sentenceButton.Text = "Luyện nói theo câu";
            this.sentenceButton.UseVisualStyleBackColor = true;
            this.sentenceButton.Click += new System.EventHandler(this.sentenceButton_Click);
            // 
            // addQuestion
            // 
            this.addQuestion.Location = new System.Drawing.Point(12, 226);
            this.addQuestion.Name = "addQuestion";
            this.addQuestion.Size = new System.Drawing.Size(87, 23);
            this.addQuestion.TabIndex = 8;
            this.addQuestion.Text = "Thêm câu hỏi";
            this.addQuestion.UseVisualStyleBackColor = true;
            this.addQuestion.Click += new System.EventHandler(this.addQuestion_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.addQuestion);
            this.Controls.Add(this.sentenceButton);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.wordButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.welcomeLabel);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label welcomeLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button wordButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.Button sentenceButton;
        private System.Windows.Forms.Button addQuestion;
    }
}