﻿namespace ketnoisqlsvv
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.word = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.statusText = new System.Windows.Forms.Label();
            this.sentenceLab = new System.Windows.Forms.Label();
            this.analyzeText = new System.Windows.Forms.Label();
            this.analyzeW = new System.Windows.Forms.Label();
            this.senBox = new System.Windows.Forms.Label();
            this.markTb = new System.Windows.Forms.Label();
            this.scoreTb = new System.Windows.Forms.Label();
            this.senBox2 = new System.Windows.Forms.RichTextBox();
            this.senCount = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.repeatButton = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nextButton = new System.Windows.Forms.PictureBox();
            this.pronounceButton = new System.Windows.Forms.PictureBox();
            this.recButton = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lineNumb = new System.Windows.Forms.Label();
            this.ChamDiem = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repeatButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pronounceButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChamDiem)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(9, 524);
            this.label1.MaximumSize = new System.Drawing.Size(300, 300);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nhấn record và bắt đầu nói, nếu gặp khó khăn hãy nghe người bản xứ nói";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // word
            // 
            this.word.AutoSize = true;
            this.word.Font = new System.Drawing.Font("Calibri", 50.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.word.ForeColor = System.Drawing.Color.Green;
            this.word.Location = new System.Drawing.Point(178, 102);
            this.word.Name = "word";
            this.word.Size = new System.Drawing.Size(187, 82);
            this.word.TabIndex = 2;
            this.word.Text = "Word";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 30F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(105, 16);
            this.label3.MaximumSize = new System.Drawing.Size(400, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(345, 49);
            this.label3.TabIndex = 3;
            this.label3.Text = "Phát âm từ sau đây";
            // 
            // statusText
            // 
            this.statusText.AutoSize = true;
            this.statusText.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.statusText.Location = new System.Drawing.Point(13, 13);
            this.statusText.Name = "statusText";
            this.statusText.Size = new System.Drawing.Size(86, 13);
            this.statusText.TabIndex = 4;
            this.statusText.Text = "Sẵn sàng ghi âm";
            // 
            // sentenceLab
            // 
            this.sentenceLab.AutoSize = true;
            this.sentenceLab.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Bold);
            this.sentenceLab.ForeColor = System.Drawing.Color.SeaGreen;
            this.sentenceLab.Location = new System.Drawing.Point(140, 102);
            this.sentenceLab.MaximumSize = new System.Drawing.Size(400, 0);
            this.sentenceLab.Name = "sentenceLab";
            this.sentenceLab.Size = new System.Drawing.Size(154, 33);
            this.sentenceLab.TabIndex = 11;
            this.sentenceLab.Text = "sentenceLab";
            // 
            // analyzeText
            // 
            this.analyzeText.AutoSize = true;
            this.analyzeText.Location = new System.Drawing.Point(438, 532);
            this.analyzeText.Name = "analyzeText";
            this.analyzeText.Size = new System.Drawing.Size(44, 13);
            this.analyzeText.TabIndex = 12;
            this.analyzeText.Text = "Analyze";
            // 
            // analyzeW
            // 
            this.analyzeW.AutoSize = true;
            this.analyzeW.Location = new System.Drawing.Point(356, 550);
            this.analyzeW.Name = "analyzeW";
            this.analyzeW.Size = new System.Drawing.Size(38, 13);
            this.analyzeW.TabIndex = 13;
            this.analyzeW.Text = "Giọng:";
            this.analyzeW.Click += new System.EventHandler(this.label2_Click);
            // 
            // senBox
            // 
            this.senBox.AutoSize = true;
            this.senBox.Font = new System.Drawing.Font("Calibri", 30F, System.Drawing.FontStyle.Bold);
            this.senBox.Location = new System.Drawing.Point(21, 86);
            this.senBox.MaximumSize = new System.Drawing.Size(400, 0);
            this.senBox.Name = "senBox";
            this.senBox.Size = new System.Drawing.Size(42, 49);
            this.senBox.TabIndex = 14;
            this.senBox.Text = "3";
            // 
            // markTb
            // 
            this.markTb.AutoSize = true;
            this.markTb.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markTb.Location = new System.Drawing.Point(24, 234);
            this.markTb.Name = "markTb";
            this.markTb.Size = new System.Drawing.Size(75, 33);
            this.markTb.TabIndex = 15;
            this.markTb.Text = "Điểm";
            this.markTb.Click += new System.EventHandler(this.markTb_Click);
            // 
            // scoreTb
            // 
            this.scoreTb.AutoSize = true;
            this.scoreTb.Font = new System.Drawing.Font("Calibri", 60F, System.Drawing.FontStyle.Bold);
            this.scoreTb.ForeColor = System.Drawing.Color.Red;
            this.scoreTb.Location = new System.Drawing.Point(12, 270);
            this.scoreTb.Name = "scoreTb";
            this.scoreTb.Size = new System.Drawing.Size(124, 97);
            this.scoreTb.TabIndex = 16;
            this.scoreTb.Text = "90";
            // 
            // senBox2
            // 
            this.senBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.senBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.senBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.senBox2.Location = new System.Drawing.Point(99, 78);
            this.senBox2.Name = "senBox2";
            this.senBox2.Size = new System.Drawing.Size(314, 140);
            this.senBox2.TabIndex = 17;
            this.senBox2.Text = "";
            // 
            // senCount
            // 
            this.senCount.AutoSize = true;
            this.senCount.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.senCount.Location = new System.Drawing.Point(12, 55);
            this.senCount.Name = "senCount";
            this.senCount.Size = new System.Drawing.Size(87, 33);
            this.senCount.TabIndex = 18;
            this.senCount.Text = "Số câu";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::ketnoisqlsvv.Properties.Resources.next_sen;
            this.pictureBox4.Location = new System.Drawing.Point(434, 149);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(87, 80);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 20;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ketnoisqlsvv.Properties.Resources.prev_sen;
            this.pictureBox3.Location = new System.Drawing.Point(18, 154);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(62, 59);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // repeatButton
            // 
            this.repeatButton.Image = global::ketnoisqlsvv.Properties.Resources._36599_200;
            this.repeatButton.Location = new System.Drawing.Point(12, 407);
            this.repeatButton.Name = "repeatButton";
            this.repeatButton.Size = new System.Drawing.Size(87, 80);
            this.repeatButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.repeatButton.TabIndex = 10;
            this.repeatButton.TabStop = false;
            this.repeatButton.Click += new System.EventHandler(this.repeatButton_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ketnoisqlsvv.Properties.Resources.Haha_Emoticons;
            this.pictureBox2.Location = new System.Drawing.Point(182, 407);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(165, 80);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ketnoisqlsvv.Properties.Resources.facebook_sad;
            this.pictureBox1.Location = new System.Drawing.Point(182, 407);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(165, 80);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // nextButton
            // 
            this.nextButton.Image = global::ketnoisqlsvv.Properties.Resources.icon_next_blue_light_braun_hi;
            this.nextButton.Location = new System.Drawing.Point(412, 397);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(97, 90);
            this.nextButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.nextButton.TabIndex = 6;
            this.nextButton.TabStop = false;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // pronounceButton
            // 
            this.pronounceButton.Image = global::ketnoisqlsvv.Properties.Resources.orange_speaker_256;
            this.pronounceButton.Location = new System.Drawing.Point(412, 275);
            this.pronounceButton.Name = "pronounceButton";
            this.pronounceButton.Size = new System.Drawing.Size(107, 92);
            this.pronounceButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pronounceButton.TabIndex = 5;
            this.pronounceButton.TabStop = false;
            this.pronounceButton.Click += new System.EventHandler(this.pronounceButton_Click);
            // 
            // recButton
            // 
            this.recButton.Image = global::ketnoisqlsvv.Properties.Resources._13652249372108434179Record_Button_Microphone_svg_hi;
            this.recButton.Location = new System.Drawing.Point(224, 275);
            this.recButton.Name = "recButton";
            this.recButton.Size = new System.Drawing.Size(90, 90);
            this.recButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.recButton.TabIndex = 1;
            this.recButton.TabStop = false;
            this.recButton.Click += new System.EventHandler(this.recButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(445, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 33);
            this.label2.TabIndex = 21;
            this.label2.Text = "Dòng";
            // 
            // lineNumb
            // 
            this.lineNumb.AutoSize = true;
            this.lineNumb.Font = new System.Drawing.Font("Calibri", 30F, System.Drawing.FontStyle.Bold);
            this.lineNumb.Location = new System.Drawing.Point(467, 97);
            this.lineNumb.MaximumSize = new System.Drawing.Size(400, 0);
            this.lineNumb.Name = "lineNumb";
            this.lineNumb.Size = new System.Drawing.Size(42, 49);
            this.lineNumb.TabIndex = 22;
            this.lineNumb.Text = "3";
            // 
            // ChamDiem
            // 
            this.ChamDiem.Image = global::ketnoisqlsvv.Properties.Resources.Check_mark;
            this.ChamDiem.Location = new System.Drawing.Point(99, 235);
            this.ChamDiem.Name = "ChamDiem";
            this.ChamDiem.Size = new System.Drawing.Size(37, 33);
            this.ChamDiem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ChamDiem.TabIndex = 23;
            this.ChamDiem.TabStop = false;
            this.ChamDiem.Click += new System.EventHandler(this.ChamDiem_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Anh-Úc Nữ 1",
            "Anh-Úc Nữ 2",
            "Anh-Canada Nữ 1",
            "Anh-Canada Nữ 2",
            "Anh-Anh Nữ 1",
            "Anh-Anh Nữ 2",
            "Anh-Anh Nam",
            "Anh-Ireland Nam",
            "Anh-Ấn Nữ 1",
            "Anh-Ấn Nữ 2",
            "Anh-Ấn Nam",
            "Anh-Mỹ Nữ 1",
            "Anh-Mỹ Nữ 2",
            "Anh-Mỹ Nam "});
            this.comboBox1.Location = new System.Drawing.Point(400, 542);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 24;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 567);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.ChamDiem);
            this.Controls.Add(this.lineNumb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.senCount);
            this.Controls.Add(this.senBox2);
            this.Controls.Add(this.scoreTb);
            this.Controls.Add(this.markTb);
            this.Controls.Add(this.senBox);
            this.Controls.Add(this.analyzeW);
            this.Controls.Add(this.analyzeText);
            this.Controls.Add(this.sentenceLab);
            this.Controls.Add(this.repeatButton);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.pronounceButton);
            this.Controls.Add(this.statusText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.word);
            this.Controls.Add(this.recButton);
            this.Controls.Add(this.label1);
            this.Name = "Form4";
            this.Text = "Form4";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form4_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repeatButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pronounceButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChamDiem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox recButton;
        private System.Windows.Forms.Label word;
        private System.Windows.Forms.Label statusText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pronounceButton;
        private System.Windows.Forms.PictureBox nextButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox repeatButton;
        private System.Windows.Forms.Label sentenceLab;
        private System.Windows.Forms.Label analyzeText;
        private System.Windows.Forms.Label analyzeW;
        private System.Windows.Forms.Label senBox;
        private System.Windows.Forms.Label markTb;
        private System.Windows.Forms.Label scoreTb;
        private System.Windows.Forms.RichTextBox senBox2;
        private System.Windows.Forms.Label senCount;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lineNumb;
        private System.Windows.Forms.PictureBox ChamDiem;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}