﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Echovoice.JSON;
using OpenNLP.Tools.Chunker;
using OpenNLP.Tools.Coreference.Similarity;
using OpenNLP.Tools.Lang.English;
using OpenNLP.Tools.NameFind;
using OpenNLP.Tools.Parser;
using OpenNLP.Tools.PosTagger;
using OpenNLP.Tools.SentenceDetect;
using OpenNLP.Tools.Tokenize;
using System.IO;

namespace ketnoisqlsvv
{    
    public class AnalyzeResult
    {
        
        private readonly string _modelPath="..\\..\\..\\OpenNLP\\";
        private AbstractTokenizer _tokenizer;
        private EnglishMaximumEntropyPosTagger _posTagger;
        private string[] TokenizeSentence(string sentence)
        {
            if (_tokenizer == null)
            {
                _tokenizer = new EnglishRuleBasedTokenizer(false);
            }

            return _tokenizer.Tokenize(sentence);
        }

        private string[] PosTagTokens(string[] tokens)
        {
            if (_posTagger == null)
            {
                _posTagger = new EnglishMaximumEntropyPosTagger(_modelPath + "EnglishPOS.nbin", _modelPath + "tagdict");
            }

            return _posTagger.Tag(tokens);
        }
        public int Len { get; set; }
        public bool Status { get; set; }
        public string[] Tokens { get; set; }
        public int WordCount { get; set; }
        public string[] type { get; set;}
        public AnalyzeResult(string str)
        {
            string[] tokens = TokenizeSentence(str);
            string[] tags = PosTagTokens(tokens);
            Tokens = tokens;
            type = tags;
            WordCount = tokens.Length;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="LinguisticsClient"/> class.
        /// </summary>        

        /// <summary>
        /// Default jsonserializer settings
        /// </summary>
        private static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings()
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        /// <summary>
        /// List analyzers synchronously.
        /// </summary>
        /// <returns>An array of supported analyzers.</returns>

    }
    public class Receiver
    {
        public int Len { get; set; }
        public int Offset { get; set; }
        public Tokens[] Tokens { get; set; }

    }
    public class Tokens
    {
        public int Len { get; set; }
        public string NormalizedToken { get; set; }
        public int Offset { get; set; }
        public string RawToken { get; set; }
    }

}
