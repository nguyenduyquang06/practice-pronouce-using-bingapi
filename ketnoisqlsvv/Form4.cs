﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.CognitiveServices.SpeechRecognition;
using System.Data.SqlClient;
using CognitiveServicesTTS;
using System.Threading;
using System.Media;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Configuration;
using OpenNLP.Tools.Chunker;
using OpenNLP.Tools.Coreference.Similarity;
using OpenNLP.Tools.Lang.English;
using OpenNLP.Tools.NameFind;
using OpenNLP.Tools.Parser;
using OpenNLP.Tools.PosTagger;
using OpenNLP.Tools.SentenceDetect;
using OpenNLP.Tools.Tokenize;

using Accord.Imaging.Filters;
using Accord.DataSets;
using Accord.Audio;
using Accord.DirectSound;
using Accord.Audio.Formats;
using Accord.Statistics.Kernels;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Math.Optimization.Losses;







namespace ketnoisqlsvv
{
    public partial class Form4 : Form
    {        

        int paraIndex = 0,id;
        string[] voice_name = { "Microsoft Server Speech Text to Speech Voice (en-AU, Catherine)",
            "Microsoft Server Speech Text to Speech Voice (en-AU, HayleyRUS)",
            "Microsoft Server Speech Text to Speech Voice (en-CA, Linda)",
            "Microsoft Server Speech Text to Speech Voice (en-CA, HeatherRUS)",
            "Microsoft Server Speech Text to Speech Voice (en-GB, Susan, Apollo)",
            "Microsoft Server Speech Text to Speech Voice (en-GB, HazelRUS)",
            "Microsoft Server Speech Text to Speech Voice (en-GB, George, Apollo)",
            "Microsoft Server Speech Text to Speech Voice (en-IE, Sean)",
            "Microsoft Server Speech Text to Speech Voice (en-IN, Heera, Apollo)",
            "Microsoft Server Speech Text to Speech Voice (en-IN, PriyaRUS)",
            "Microsoft Server Speech Text to Speech Voice (en-IN, Ravi, Apollo)",
            "Microsoft Server Speech Text to Speech Voice (en-US, ZiraRUS)",
            "Microsoft Server Speech Text to Speech Voice (en-US, JessaRUS)",
            "Microsoft Server Speech Text to Speech Voice (en-US, BenjaminRUS)"};
        string voice_cur;
        AnalyzeDialog test;       
        float confidence = 1;
        SqlConnection connect;
        private static int isWord;        
        private MicrophoneRecognitionClient micClient;
        [DllImport("winmm.dll")]
        private static extern long mciSendString(string command, StringBuilder retstrign, int Returnlength, IntPtr callback);                

        private void CreateMicrophoneRecoClient()
        {
            this.micClient = SpeechRecognitionServiceFactory.CreateMicrophoneClient(
                SpeechRecognitionMode.ShortPhrase,
                this.DefaultLocale,
                this.SubscriptionKey);
            this.micClient.AuthenticationUri = "";

            // Event handlers for speech recognition results
            this.micClient.OnMicrophoneStatus += this.OnMicrophoneStatus;
            this.micClient.OnPartialResponseReceived += this.OnPartialResponseReceivedHandler;
            this.micClient.OnResponseReceived += this.OnMicDictationResponseReceivedHandler;
            this.micClient.OnResponseReceived += this.OnMicShortPhraseResponseReceivedHandler;
        }
        private void OnPartialResponseReceivedHandler(object sender, PartialSpeechResponseEventArgs e)
        {
            Invoke(
             (Action)(() =>
                  {
                      statusText.Text = e.PartialResult;
                  }));
              
        }
        private void OnMicShortPhraseResponseReceivedHandler(object sender, SpeechResponseEventArgs e)
        {
            Invoke((Action)(() =>
            {                
                this.micClient.EndMicAndRecognition();                                
                this.WriteResponseResult(e);
                mciSendString("save recsound test.wav", null, 0, IntPtr.Zero);
                mciSendString("close recsound", null, 0, IntPtr.Zero);
                repeatButton.Visible = true;
                if (String.Equals(word.Text, Regex.Replace(statusText.Text, "\\.", ""), StringComparison.OrdinalIgnoreCase))
                {

                    pictureBox1.Visible = false;
                    pictureBox2.Visible = true;
                    //MessageBox.Show("Phát âm chính xác");
                    if (Form4.isWord==0)
                    {
                        scoreTb.Text = mark_Calculation("test.wav").ToString();
                    }                                  
                }
                else
                {
                    //MessageBox.Show("Bạn đã nói nhầm thành " + statusText.Text);
                    pictureBox2.Visible = false;
                    pictureBox1.Visible = true;
                    if (Form4.isWord==0)
                    {
                        scoreTb.Text = "0";
                    }
                    
                }
                if (Form4.isWord == 1)
                {
                    scoreTb.Text = test.markSentence(0, Regex.Replace(statusText.Text, "\\.", ""), confidence).ToString();
                    //markSentence(statusText.Text, confidence);
                }
                if (Form4.isWord == 2)
                {                         
                    try
                    {
                        scoreTb.Text = test.markSentence(paraIndex, Regex.Replace(statusText.Text, "\\.", ""), confidence).ToString();
                        if (paraIndex == test.LineCount - 1)
                        {
                            test.MarkPara = 0;
                            MessageBox.Show(test.MarkPara.ToString(), "Tổng Điểm");
                        }
                        //markSentence(statusText.Text, confidence);
                    }
                    catch
                    {

                    }
                    //scoreTb.Text = test.markSentence(paraIndex, Regex.Replace(statusText.Text, "\\.", ""), confidence).ToString();
                    //if (paraIndex == test.LineCount - 1)
                    //{
                    //    test.MarkPara = 0;
                    //    MessageBox.Show(test.MarkPara.ToString(), "Tổng Điểm");
                    //}
                    ////markSentence(statusText.Text, confidence);
                }
                scoreTb.Visible = true;
                statusText.Text = "Sẵn sàng ghi âm";                
                
            }));
        }
        private void OnMicDictationResponseReceivedHandler(object sender, SpeechResponseEventArgs e)
        {
            if (e.PhraseResponse.RecognitionStatus == RecognitionStatus.EndOfDictation ||
                e.PhraseResponse.RecognitionStatus == RecognitionStatus.DictationEndSilenceTimeout)
            {
                // we got the final result, so it we can end the mic reco.  No need to do this
                // for dataReco, since we already called endAudio() on it as soon as we were done
                // sending all the data.
                Invoke(
                    (Action)(() =>
                    {
                        // we got the final result, so it we can end the mic reco.  No need to do this
                        // for dataReco, since we already called endAudio() on it as soon as we were done
                        // sending all the data.
                        this.micClient.EndMicAndRecognition();                        
                        
                        statusText.Text = "Sẵn sàng ghi âm";
                        
                    }));
            }
            this.WriteResponseResult(e);
        }
        private void OnMicrophoneStatus(object sender, MicrophoneEventArgs e)
        {
            
            Invoke(
                    (Action)(() =>
                    {
                        if (e.Recording)
                        {
                            statusText.Text = "Bắt đầu nói.";
                        }

                    }));
        }

        private void WriteResponseResult(SpeechResponseEventArgs e)
        {
            if (e.PhraseResponse.Results.Length == 0)
            {
                Invoke(
                     (Action)(() =>
                     {
                        statusText.Text = "Hãy nói gì đó, chúng tôi chưa nghe bạn nói gì";
                      }));
                
            }
            else
            {
                //Console.WriteLine("********* Final n-BEST Results *********");
                //for (int i = 0; i < e.PhraseResponse.Results.Length; i++)
                //{
                //    Console.WriteLine(
                //        "[{0}] Confidence={1}, Text=\"{2}\"",
                //        i,
                //        e.PhraseResponse.Results[i].Confidence,
                //        e.PhraseResponse.Results[i].DisplayText);
                //}

                //Console.WriteLine();
                confidence = (Convert.ToSingle(e.PhraseResponse.Results[0].Confidence) + 2) / 3;               
                Invoke(
                  (Action)(() =>
                   {
                     statusText.Text = e.PhraseResponse.Results[0].DisplayText;
                   }));                
            }
        }



        public void StartRecognition()
        {
            this.micClient.StartMicAndRecognition();

        }
        private string DefaultLocale
        {
            get { return "en-US"; }
        }

        public string SubscriptionKey
        {
            get
            {
                return ConfigurationManager.AppSettings["SubscriptionKey"];
            }
        }

        public Form4(int isWord)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Local Database Files"));
            connect= new SqlConnection(@ConfigurationManager.AppSettings["DataStringConnection"]);
            Form4.isWord = isWord;
            InitializeComponent();            
            string sql;
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            repeatButton.Visible = false;
            comboBox1.SelectedIndex = 12;
            connect.Open();
            if (Form4.isWord==0)
            {
                sql = "SELECT TOP 1 Tu,ID FROM QuestionByWord ORDER BY NEWID()";                
            }            
            else if (Form4.isWord == 1)
            {
                sql = "SELECT TOP 1 Tu,ID FROM QuestionBySentence ORDER BY NEWID()";                
            }
            else
            {
                sql = "SELECT TOP 1 Tu,ID FROM QuestionByDialog ORDER BY NEWID()";
            }
            SqlCommand cmd = new SqlCommand(sql, connect);
            SqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                if (Form4.isWord==0)
                {
                    word.Text = DR[0].ToString();
                    id = Convert.ToInt32(DR[1].ToString());
                    sentenceLab.Visible = false;
                    analyzeText.Visible = false;
                    senBox.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    ChamDiem.Visible = false;
                    lineNumb.Visible = false;
                    senBox.Visible = false;
                    senCount.Visible = false;
                    label2.Visible = false;
                    senBox2.Visible = false;
                }
                else if (Form4.isWord == 1)
                {
                    test = new AnalyzeDialog(DR[0].ToString());
                    sentenceLab.Text = DR[0].ToString();
                    id = Convert.ToInt32(DR[1].ToString());
                    senBox2.Visible = false;
                    //sentenceLab.Text = "I'm not gonna lie you anymore";
                    word.Visible = false;
                    word.Text = sentenceLab.Text;
                    label3.Text = "Phát âm câu sau đây";                    
                    analyzeText.Text = "";
                    //analyzedRes = new AnalyzeResult(filterString(sentenceLab.Text)+" .");
                    senBox.Visible = false;
                    senBox.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    ChamDiem.Visible = false;
                    lineNumb.Visible = false;
                    senBox.Visible = false;
                    senCount.Visible = false;
                    label2.Visible = false;
                    senBox2.Visible = false;
                }
                else
                {
                    test = new AnalyzeDialog(DR[0].ToString());
                    id = Convert.ToInt32(DR[1].ToString());
                    //test = new AnalyzeDialog("Hello, how are you today ?\nI'm good. How about you ?\nMe too.");
                    senBox.Text = (test.LineCount).ToString();
                    word.Text = test.Sentences[paraIndex];
                    senBox2.Text = test.SentenceInDialog();
                    Highlight.HighlightLine(senBox2, paraIndex, Color.Aquamarine);
                    lineNumb.Text = "0";
                    senBox2.ReadOnly = true;
                    //senBox.Text = DR[0].ToString();
                    word.Visible = false;
                    sentenceLab.Visible = false;
                    label3.Text = "Phát đoạn văn sau đây";
                    analyzeText.Text = "";
                    //analyzedRes = new AnalyzeResult(filterString(senBox.Text) + " .");
                    senBox.Visible = true;
                }                
            }
            scoreTb.Visible = false;
            connect.Close();
            
        }


        //speech to text api

        private void recButton_Click(object sender, EventArgs e)
        {
            mciSendString("open new Type waveaudio alias recsound", null, 0, IntPtr.Zero);
            mciSendString("record recsound", null, 0, IntPtr.Zero);            
            this.CreateMicrophoneRecoClient();
            micClient.StartMicAndRecognition();
        }

        //text to speech api
        private static void PlayAudio(object sender, GenericEventArgs<Stream> args)
        {
            Console.WriteLine(args.EventData);

            // For SoundPlayer to be able to play the wav file, it has to be encoded in PCM.
            // Use output audio format AudioOutputFormat.Riff16Khz16BitMonoPcm to do that.
            SoundPlayer player = new SoundPlayer(args.EventData);
            player.PlaySync();
            args.EventData.Dispose();
        }

        /// <summary>
        /// Handler an error when a TTS request failed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GenericEventArgs{Exception}"/> instance containing the event data.</param>
        private static void ErrorHandler(object sender, GenericEventArgs<Exception> e)
        {
            Console.WriteLine("Unable to complete the TTS request: [{0}]", e.ToString());
        }
        private void pronounceButton_Click(object sender, EventArgs e)
        {
            string accessToken;
            Authentication auth = new Authentication(SubscriptionKey);
            try
            {
                accessToken = auth.GetAccessToken();
                Console.WriteLine("Token: {0}\n", accessToken);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed authentication.");
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.Message);
                return;
            }            

            string requestUri = "https://speech.platform.bing.com/synthesize";
            var cortana = new Synthesize();

            cortana.OnAudioAvailable += PlayAudio;
            cortana.OnError += ErrorHandler;

            // Reuse Synthesize object to minimize latency
            Invoke(
             (Action)(() =>
             {
                 cortana.Speak(CancellationToken.None, new Synthesize.InputOptions()
                 {
                     RequestUri = new Uri(requestUri),
                     // Text to be spoken.
                     Text = word.Text,
                     VoiceType = CognitiveServicesTTS.Gender.Female,
                     // Refer to the documentation for complete list of supported locales.
                     Locale = "en-US",
                     // You can also customize the output voice. Refer to the documentation to view the different
                     // voices that the TTS service can output.
                     VoiceName = voice_cur,
                     // Service can return audio in different output format.
                     OutputFormat = AudioOutputFormat.Riff16Khz16BitMonoPcm,
                     AuthorizationToken = "Bearer " + accessToken,
                 }).Wait();
             }));           
        }

        
        private void nextButton_Click(object sender, EventArgs e)
        {
            connect.Open();
            string sql;            
            if (Form4.isWord==0)
            {
                sql = "SELECT TOP 1 Tu,ID FROM QuestionByWord ORDER BY NEWID()";
            }
            else if (Form4.isWord == 1)
            {
                sql = "SELECT TOP 1 Tu,ID FROM QuestionBySentence ORDER BY NEWID()";                
            }
            else 
            {
                sql = "SELECT TOP 1 Tu,ID FROM QuestionByDialog ORDER BY NEWID()";
            }

            
            SqlCommand cmd = new SqlCommand(sql, connect);
            SqlDataReader DR = cmd.ExecuteReader();            
            while (DR.Read())
            {
                if (Form4.isWord==0)
                {
                    word.Text = DR[0].ToString();
                    sentenceLab.Visible = false;   
                    if (Convert.ToInt32(DR[1].ToString())==id)
                    {
                        //cmd = new SqlCommand(sql, connect);
                        DR.Close();
                        DR = cmd.ExecuteReader();
                        continue;
                    }
                    id = Convert.ToInt32(DR[1].ToString());
                }
                else if (Form4.isWord == 1)
                {
                    if (Convert.ToInt32(DR[1].ToString()) == id)
                    {
                        //cmd = new SqlCommand(sql, connect);
                        DR.Close();
                        DR = cmd.ExecuteReader();
                        continue;
                    }
                    paraIndex = 0;
                    test = new AnalyzeDialog(DR[0].ToString());
                    id = Convert.ToInt32(DR[1].ToString()); 
                    sentenceLab.Text = DR[0].ToString();
                    word.Visible = false;
                    word.Text = sentenceLab.Text;
                    label3.Text = "Phát âm câu sau đây";
                    analyzeText.Text = "";                    
                    //analyzedRes = new AnalyzeResult(filterString(sentenceLab.Text) +" .");                                                
                }
                else
                {
                    if (Convert.ToInt32(DR[1].ToString()) == id)
                    {
                        //cmd = new SqlCommand(sql, connect);
                        DR.Close();
                        DR = cmd.ExecuteReader();
                        continue;
                    }
                    id = Convert.ToInt32(DR[1].ToString());
                    paraIndex = 0;                                                       
                    test = new AnalyzeDialog(DR[0].ToString());
                    //test = new AnalyzeDialog("Hello, how are you today ?\nI'm good. How about you ?\nMe too.");
                    senBox.Text = test.LineCount.ToString();
                    word.Text = test.Sentences[paraIndex];
                    senBox2.Text = test.SentenceInDialog();
                    Highlight.HighlightLine(senBox2, paraIndex, Color.Aquamarine);
                    lineNumb.Text = "1";
                    senBox2.ReadOnly = true;                                      
                    analyzeText.Text = "";                    
                    senBox.Visible = true;
                }
            }
            connect.Close();           
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            repeatButton.Visible = false;
            scoreTb.Visible = false;
        }

        String filterString(String s)
        {
            s = Regex.Replace(s, "\\,", "");
            s = Regex.Replace(s, "\\.", "");
            s = Regex.Replace(s, "\\?", "");
            s = Regex.Replace(s, "\\:", "");
            s = Regex.Replace(s, "\"", "");
            return s;
        }

        public float markPara(string question,string voice, float conf)
        {

            return 1;
        }

        public int markSentence(AnalyzeResult pattern_res,string voice,float conf)
        {
            AnalyzeResult voice_res;
            int i = 0, j = 0;
            float result=0,max=0;
            float[] weight = new float[50];
            int i_prev = -1, j_prev =-1 ;
            SqlCommand cmd;
            SqlDataReader DR;
            string sql;
            //analyzeW.Text = "";                        
            voice_res = new AnalyzeResult(filterString(voice));
            //pattern_res = new AnalyzeResult(filterString(pattern) + " .");
            int comp = pattern_res.WordCount - voice_res.WordCount;
            for (i = 0; i < pattern_res.WordCount; i++)
            {
                connect.Open();
                sql = "SELECT TOP 1 Weight FROM WeightTag WHERE Tag='"+ pattern_res.type[i]+ "' ORDER BY NEWID()";
                cmd = new SqlCommand(sql, connect);
                DR = cmd.ExecuteReader();
                while (DR.Read())
                {
                    max += Convert.ToSingle(DR[0]);
                    weight[i] = Convert.ToSingle(DR[0]);
                    //analyzeW.Text += DR[0].ToString() + " ";

                }
                connect.Close();
            }
            i = 0;
                while (i < pattern_res.WordCount && j < voice_res.WordCount)
                {
                    if (String.Equals(pattern_res.Tokens[i], voice_res.Tokens[j], StringComparison.OrdinalIgnoreCase))
                    {
                        result += weight[i];
                        i++;
                        j++;
                    }
                    else if (i_prev!=i)
                    {
                        result -= weight[i];
                        if (i<pattern_res.WordCount-1)
                        {
                            if (String.Equals(pattern_res.Tokens[i + 1], voice_res.Tokens[j], StringComparison.OrdinalIgnoreCase))
                            {
                                i++;
                            }
                            else
                            {
                                i_prev = i;
                                j_prev = ++j;
                            }
                        }
                        else
                        {
                            i_prev = i;
                            j_prev = ++j;
                        }
                }                    
                    else
                    {
                        j++;
                    }                    
                    if (j == voice_res.WordCount)
                    {
                        i++;
                        j = j_prev;
                    }
                }
            //}            
            result = result < 0 ? 0 : result; //âm điểm thì cho 0
            return (int) (result/max * 100);
        }
        

        private void prevButton_Click(object sender, EventArgs e)
        {
            
        }
        private void Form4_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
          if (MessageBox.Show("Thật sự muốn thoát ?", "Exit",
              MessageBoxButtons.YesNo) == DialogResult.Yes)
                {                                  
                    Environment.Exit(Environment.ExitCode);
                }
        }

        private void repeatButton_Click(object sender, EventArgs e)
        {
            mciSendString("play test.wav", null, 0, IntPtr.Zero); 
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }


        private int mark_Calculation(string user_wav)
        {
            connect.Open();
            string URL = ""; string sql;
            sql = "SELECT TOP 1 URL FROM QuestionByWord WHERE Tu='" + word.Text + "'";
            SqlCommand cmd = new SqlCommand(sql, connect);
            SqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                URL = DR[0].ToString();
            }
            connect.Close();

            WaveDecoder sourceDecoder1 = new WaveDecoder(user_wav);
            WaveDecoder sourceDecoder2 = new WaveDecoder(URL);
            Signal source1 = sourceDecoder1.Decode();
            Signal source2 = sourceDecoder2.Decode();
            MFCC b1 = new MFCC();
            MFCC b2 = new MFCC();
            double[][] c1 = b1.ProcessSignal(source1);
            double[][] c2 = b2.ProcessSignal(source2);
            double[][] input = Concate(c1, c2);
            int[] output = new int[c1.Length + c2.Length];
            for (int k = 0; k < c1.Length + c2.Length; k++)
            {
                if (k < c1.Length) output[k] = 0;
                else output[k] = 1;
            }

            var smo = new SequentialMinimalOptimization<DynamicTimeWarping>()
            {
                Complexity = 1.5,

                // Set the parameters of the kernel
                Kernel = new DynamicTimeWarping(alpha: 1, degree: 1)
            };

            // And use it to learn a machine!
            var svm = smo.Learn(input, output);
            // Now we can compute predicted values
            bool[] predicted = svm.Decide(input);

            // And check how far we are from the expected values
            double error = new ZeroOneLoss(output).Loss(predicted); // error will be 0.0
            int mark = 0;
            if (error <= 0.1) mark = 10;
            else if (error > 0.1 && error <= 0.2) mark = 9;
            else if (error > 0.2 && error <= 0.3) mark = 8;
            else mark = 7;
            sourceDecoder1.Close();
            sourceDecoder2.Close();
            return mark*10;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void markTb_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (paraIndex<test.LineCount-1)
            {
                lineNumb.Text = (++paraIndex+1).ToString();
                Highlight.HighlightLine(senBox2, paraIndex, Color.Aquamarine);
                word.Text = test.Sentences[paraIndex];
            }            
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (paraIndex > 0)
            {
                lineNumb.Text = (--paraIndex+1).ToString();
                Highlight.HighlightLine(senBox2, paraIndex, Color.Aquamarine);
                word.Text = test.Sentences[paraIndex];
            }
        }

        private void ChamDiem_Click(object sender, EventArgs e)
        {
            test.MarkPara = 0;
            scoreTb.Text = test.MarkPara.ToString();
            scoreTb.Visible = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            voice_cur = voice_name[comboBox1.SelectedIndex];
        }

        private static double[][] Concate(double[][] array1, double[][] array2)
        {
            double[][] result = new double[array1.Length + array2.Length][];
            array1.CopyTo(result, 0);
            array2.CopyTo(result, array1.Length);
            return result;
        }
    }
}
