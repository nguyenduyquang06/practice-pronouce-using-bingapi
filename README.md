# Practice English pronounce with Bingspeech API & OpenNLP and MFCC

# INTRO
- Practice English pronounce by words, sentence and paragraph with a nice UI design.
- Extract MFCC feature, apply SVM algorithm for scoring to calculate for scoring
# TECHNOLOGY 
- C# for Backend Programming
- bunifu for UI design
- SQL Server for database
- MFCC .NET for extracting MFCC feature
# Algorithm Flowchart
### Algorithm for scoring
<p>
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/giai%20thuat_zpspwii1jii.jpg">
</p>
### Algorithm for extract MFCC feature
<p>
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/mfcc_zpsggssm7cu.jpg">
</p>

# HOW TO RUN
- Step 1: Change your API Bing Speech key in source code
- Step 2: Install SQL server
- Step 3: Build project

# Presentation
### Main UI

<p align="center">
<img align="center" src="https://i1274.photobucket.com/albums/y433/duyquang6/main_ui_zpsbjghiewz.jpg">
</p>

### Login screen

<p align="center"> 
<img align="center" src="https://i1274.photobucket.com/albums/y433/duyquang6/login_zpsipgxuhgj.jpg">
</p>

### Paragraph test

<p align="center">
<img align="center" src="https://i1274.photobucket.com/albums/y433/duyquang6/para2_zpsljmlhho9.jpg">
</p>